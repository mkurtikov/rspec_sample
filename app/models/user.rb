class User < ActiveRecord::Base
  has_secure_password

  has_many :sessions
  validates :email, uniqueness: true, presence: true
end
